////
////  WindowDataServiceInteractorTests.swift
////  DrivmaticsLiteTests
////
////  Created by Bruno on 15/07/19.
////  Copyright © 2019 Bruno Raphael. All rights reserved.
////
//
//import XCTest
//@testable import DrivmaticsLite
//
//class WindowDataInteractorTests: XCTestCase {
//    
//    var data:WindowDataEntity?
//    
//    override func setUp() {
//        super.setUp()
//        self.data = nil
//    }
//    
//    override func tearDown() {
//        super.tearDown()
//        self.data = nil
//    }
//    
//    func testsStart() {
//        
//        XCTAssertNil(self.data, "Success")
//        
//        let interactor = WindowDataInteractorMock()
//        interactor.delegate = self
//        interactor.start()
//        
//        XCTAssertNotNil(self.data)
//        
//    }
//    
//}
//
//
//extension WindowDataInteractorTests:WindowDataInteractorDelegate {
//    
//    func fetched(data: WindowDataEntity) {
//        self.data = data
//    }
//    
//}
//
//
//class WindowDataInteractorMock:WindowDataInteractorInput {
//    
//    var deviceSettings: DeviceSettingsInteractorInput
//    var delegate:WindowDataInteractorDelegate?
//    
//    init(deviceSettings: DeviceSettingsInteractorInput) {
//        self.deviceSettings = deviceSettings
//    }
//    
//    func start() {
//        
//        self.deviceSettings.fetchDeviceSettings()
//        
//        //        let deviceData = DeviceSettingsItem.init(addressIP: "-", appVersion: "-", deviceModel: "-", systemVersion: "-", uuid: "-", screenResolution: "-", manufactirer: "-")
//        //
//        //        let locationItem = LocationItem.init(latitude: 0.0, longitude: 0.0, altitude: 0.0, horizontalAccuracy: 0.0, speed: 0.0)
//        //
//        //        let data = WindowDataEntity.make(deviceData:deviceData , locationItem: locationItem)
//        //
//        //        self.delegate?.fetched(data: data)
//    }
//}
//
//extension WindowDataInteractorMock : DeviceSettingsInteractorOutput {
//    
//    func fetchedGeneral(settings: DeviceSettingsItem) {
//        self.deviceSettings = settings
//    }
//}
//
//final class WindowDataInteractorMockBuilder {
//    
//}
