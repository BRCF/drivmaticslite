//
//  AppDelegate.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 23/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    self.window = UIWindow(frame: UIScreen.main.bounds)
    
    guard let safeWindow = self.window else {
      return true
    }
    
    Dispatch().startAppIn(safeWindow)
    Dispatch().isIdleTimer(true)
    
    Fabric.with([Crashlytics.self])
    return true
  }
  
}

