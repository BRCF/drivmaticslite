//
//  GPSScreenViewControllerBuilder.swift
//  Drivmatics
//
//  Created by Bruno on 13/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class GPSScreenViewControllerBuilder {
    
    static func make(wireframe:GPSScreenWireframe) -> GPSScreenViewController {
        
        let startVC = GPSScreenViewController(nibName: String(describing: GPSScreenViewController.self), bundle: nil)
        var presenter = GPSScreenViewControllerPresenterBuilder.make(wireframe)
        startVC.presenter = presenter
        presenter.output = startVC
        
        return startVC
    }
}
