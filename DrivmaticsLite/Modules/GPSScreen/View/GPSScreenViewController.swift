//
//  GPSScreenViewController.swift
//  Drivmatics
//
//  Created by Bruno Raphael Castilho de Freitas on 12/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import UIKit

class GPSScreenViewController: UIViewController {
    
    var presenter:GPSScreenViewControllerPresenterInput?
    var device:DeviceMotionItem?
    var index = 32
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var xValue: UILabel!
    @IBOutlet weak var yValue: UILabel!
    @IBOutlet weak var zValue: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var longLabel: UILabel!
    @IBOutlet weak var buildLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let unwrapVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            self.buildLabel.text = "Build \(unwrapVersion)"
        }
    }
    
    @IBAction func startButtonTouched(_ sender: Any) {
        self.presenter?.didTouchGPSButton()
    }
    
    @IBAction func sensorSelected(_ sender: UISegmentedControl) {
        
        
        switch sender.selectedSegmentIndex {
            
        case 0:
            self.index = 0
            
        case 1:
            self.index = 1
            
        case 2:
            self.index = 2
            
        case 3:break
            
        default:
            break
        }
        
    }
    
    private func updateSensor() {
        
        guard let sensor = self.device else { return }
        
        
        switch self.index {
        case 0:
            self.setSensor(x: sensor.acceleration.x, y:sensor.acceleration.y, z:sensor.acceleration.z)
        case 1:
            self.setSensor(x: sensor.rotation.x, y:sensor.rotation.y, z:sensor.rotation.z)
        case 2:
            self.setSensor(x: sensor.magneticField.x, y:sensor.magneticField.y, z:sensor.magneticField.z)
            
        default:break
        }
    }
    
    private func setSensor(x:Double, y:Double, z:Double) {
        self.xValue.text = String(x)
        self.yValue.text = String(y)
        self.zValue.text = String(z)
    }
}


extension GPSScreenViewController:GPSScreenViewControllerPresenterOutput {
    
    
    func fetched(date:String) {
        self.timerLabel.text = date
    }
    
    func fetched(_ motion: DeviceMotionItem) {
        self.device = motion
        self.updateSensor()
    }
    
    
    func fetched(_ GPS: locatorEntity) {
        self.speedLabel.text = String(GPS.speed)
        self.latLabel.text = String(GPS.latitude)
        self.longLabel.text = String(GPS.longitude)
    }
    
    
    func fetched(_ GPS: WindowType) {
        speedLabel.text = GPS.speed ?? ""
        self.latLabel.text = GPS.latitude ?? ""
        self.longLabel.text = GPS.longitude ?? ""
    }
    
    
    func changeButtonFor(connected: Bool) {
        
        if connected {
            self.button.setImage(UIImage.init(named: "greenPower"), for: .normal)
        }else {
            self.button.setImage(UIImage.init(named: "power"), for: .normal)
        }
    }
}
