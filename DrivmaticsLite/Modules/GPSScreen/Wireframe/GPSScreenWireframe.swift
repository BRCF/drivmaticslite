//
//  GPSScreenWireframe.swift
//  Drivmatics
//
//  Created by Bruno on 13/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import MapKit

final class GPSScreenWireframe {
    
    var controller:GPSScreenViewController?
   var navigationController:UINavigationController?
    
    init() {
        self.controller = GPSScreenViewControllerBuilder.make(wireframe: self)
    }
    
  func make()-> UINavigationController {
    
    self.navigationController = UINavigationController(rootViewController:self.controller!)
    
    return self.navigationController!
  }
    
}

