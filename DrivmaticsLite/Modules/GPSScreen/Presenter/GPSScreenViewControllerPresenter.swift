//
//  GPSScreenViewControllerPresenter.swift
//  Drivmatics
//
//  Created by Bruno on 13/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol GPSScreenViewControllerPresenterInput {
    func didTouchGPSButton()
    func viewDidLoad()
    var output:GPSScreenViewControllerPresenterOutput? {get set}
}

protocol GPSScreenViewControllerPresenterOutput {
    func changeButtonFor(connected:Bool)
    func fetched(_ GPS:locatorEntity)
    func fetched(_ motion:DeviceMotionItem)
    func fetched(date:String)
}

final class GPSScreenViewControllerPresenter {
    
    let wireframe:GPSScreenWireframe
    var output:GPSScreenViewControllerPresenterOutput?
    var deviceSensorInteractor:DeviceSensorsInteractorInput
    var run:Bool = false
    let locatorManager = LocatorManager.instance
    let motionManager = MotionManager.instance
    
    init(deviceSensorInteractor:DeviceSensorsInteractorInput, wireframe:GPSScreenWireframe) {
        self.wireframe = wireframe
        self.deviceSensorInteractor = deviceSensorInteractor
        self.locatorManager.output?.addDelegate(self)
        self.motionManager.delegate?.addDelegate(self)
    }
    
    func viewDidLoad() {
        
        self.didTouchGPSButton()
        self.output?.fetched(date: DateFormat.getDateFromTimeStamp(timeStamp: Date()))
        
        
        /*
         self.timer = Timer(fire: Date(), interval:0.01, repeats: true,
         block: { (timer) in
         if let data = self.motionManager.deviceMotion {
         
         self.delegate?.invoke({$0.fetched(data:SensorItemBuilder.makeWith(deviceMotion:data))})
         
         }
         })
 
            */
        
    }
    
    private func start() {
        self.output?.changeButtonFor(connected: true)
    }
    
    private func stop() {
        self.output?.changeButtonFor(connected: false)
    }
    
    private func resolveGPSButton() {
        
        self.run = !self.run
        
        if self.run {
            self.start()
        }else {
            self.stop()
        }
    }
}

extension GPSScreenViewControllerPresenter:GPSScreenViewControllerPresenterInput {
    
    func didTouchGPSButton() {
        self.resolveGPSButton()
        self.deviceSensorInteractor.startWindow()
    }
}


extension GPSScreenViewControllerPresenter : LocatorManagerOutput {
    
    func fetchedLocation(entity: locatorEntity) {
        self.output?.fetched(entity)
    }
    
    func failureToRequestLocation() {}
}

extension GPSScreenViewControllerPresenter : MotionManagerDelegate {
    
    func fetched(data: DeviceMotionItem) {
        self.output?.fetched(data)
    }
}

extension GPSScreenViewControllerPresenter : DeviceSensorsInteractorDelegate {
    
    func fetched(GPS: WindowType) {
//        self.output?.fetched(GPS)
    }
}
