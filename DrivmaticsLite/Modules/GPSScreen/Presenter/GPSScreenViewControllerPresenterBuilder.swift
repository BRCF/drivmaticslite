//
//  GPSScreenViewControllerPresenterBuilder.swift
//  DrivmaticsLite
//
//  Created by Bruno on 16/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class GPSScreenViewControllerPresenterBuilder {
    
    static func make(_ wireframe:GPSScreenWireframe) -> GPSScreenViewControllerPresenterInput {
        
        let sensorInteractor = DeviceSensorsInteractorBuilder.make()
        
        let presenter = GPSScreenViewControllerPresenter.init(deviceSensorInteractor: sensorInteractor, wireframe: wireframe)
        
        sensorInteractor.delegate = presenter
        
        return presenter
    }
}
