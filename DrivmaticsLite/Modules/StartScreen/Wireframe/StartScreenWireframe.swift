//
//  StartScreenWireframe.swift
//  Drivmatics
//
//  Created by Bruno on 13/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import MapKit

final class StartScreenWireframe {
    
    var controller:StartScreenViewController?
    var navigationController:UINavigationController?
  
    init() {
        self.controller = StartScreenViewControllerBuilder.make(wireframe: self)
    }
    
    func make()-> UINavigationController {
      
        self.navigationController = UINavigationController(rootViewController:self.controller!)
      
        return self.navigationController!
    }
  
  func moveToGPSSCreen() {
    let gpsVC = GPSScreenWireframe().make()
    self.navigationController?.present(gpsVC, animated: true, completion: nil)
  }
  
}

