//  StartScreenViewControllerPresenter.swift
//  Drivmatics
//
//  Created by Bruno on 13/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol StartScreenViewControllerPresenterInput {
  
  func endEditing(text:String, isPass:Bool)
  func loginAction()
  
}

protocol  StartScreenViewControllerPresenterOutput {
}

final class StartScreenViewControllerPresenter:StartScreenViewControllerPresenterInput {
  
  let wireframe:StartScreenWireframe
  var output:StartScreenViewControllerPresenterOutput?
  var username:String = ""
  var pass:String = ""
  
  init(wireframe:StartScreenWireframe) {
    self.wireframe = wireframe
  }
  
  func endEditing(text:String, isPass:Bool) {
    if isPass {
      self.pass = text
    }else {
      self.username = text
    }
  }
  
  func loginAction() {
    let login = LoginInteractor()
    login.processLogin(name: self.username.trimmingCharacters(in: .whitespaces), pass: self.pass.trimmingCharacters(in: .whitespaces)) { (token) in
        
        if (token != nil) {
             self.wireframe.moveToGPSSCreen()
        }
    
    }
  }
  
}


extension StartScreenViewControllerPresenter {
  
  static func make(wireframe:StartScreenWireframe)-> StartScreenViewControllerPresenter {
    let startScreen = StartScreenViewControllerPresenter.init(wireframe: wireframe)
    return startScreen
  }
}

