//
//  StartScreenViewControllerBuilder.swift
//  Drivmatics
//
//  Created by Bruno on 13/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class StartScreenViewControllerBuilder {
    
    static func make(wireframe:StartScreenWireframe) -> StartScreenViewController {
        
        let startVC = StartScreenViewController(nibName: String(describing: StartScreenViewController.self), bundle: nil)
        let presenter = StartScreenViewControllerPresenter.make(wireframe:wireframe)
        startVC.presenter = presenter
        presenter.output = startVC
        
        
        return startVC
    }
}
