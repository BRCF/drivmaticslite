//
//  StartScreenViewController.swift
//  Drivmatics
//
//  Created by Bruno Raphael Castilho de Freitas on 12/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import UIKit

class StartScreenViewController: UIViewController {
  
  @IBOutlet weak var passwordTextField: UITextField!
  var presenter:StartScreenViewControllerPresenter?
  @IBOutlet weak var usernameTextField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    self.passwordTextField.delegate = self
    self.usernameTextField.delegate = self
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification , object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification , object: nil)
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    view.endEditing(true)
  }
  
  @IBAction func loginAction(_ sender: Any) {
    self.presenter?.loginAction()
    
  }
}

extension StartScreenViewController : StartScreenViewControllerPresenterOutput {}



//MARK: - KEYBOARD SET FRAME -
extension StartScreenViewController {
  @objc func keyboardWillShow(notification: NSNotification) {
    let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.height
    UIView.animate(withDuration: 0.1, animations: { () -> Void in
      self.view.window?.frame.origin.y = -1 * (keyboardHeight! - 150)
      self.view.layoutIfNeeded()
    })
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    UIView.animate(withDuration: 0.1, animations: { () -> Void in
      self.view.window?.frame.origin.y = 0
      self.view.layoutIfNeeded()
    })
  }
}



//MARK: - TEXTFIELD DELEGATE -
extension StartScreenViewController:UITextFieldDelegate {
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
  
    if textField == self.passwordTextField {
      self.presenter?.endEditing(text: textField.text ?? "", isPass: true)
    } else {
      self.presenter?.endEditing(text: textField.text ?? "", isPass: false)
    }
    
    
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
