//
//  WindowDataServiceInteractor.swift
//  DrivmaticsLite
//
//  Created by Bruno on 15/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol WindowDataInteractorInput {
    func start()
    var deviceSettings:DeviceSettingsInteractorInput {get}
    var location:LocatorInteractorInput {get}
    var delegate:WindowDataInteractorDelegate? {get set}
}

protocol WindowDataInteractorDelegate {
    func fetched(data:WindowDataEntity)
}

final class WindowDataInteractor:WindowDataInteractorInput {
    
    var delegate: WindowDataInteractorDelegate?
    var location: LocatorInteractorInput
    var deviceSettings:DeviceSettingsInteractorInput
    private var deviceItem:DeviceSettingsItem?
    private var locationItem:LocationItem?
    
    init(deviceSettings:DeviceSettingsInteractorInput, location: LocatorInteractorInput) {
        self.deviceSettings = deviceSettings
        self.location = location
    }
    
    func start() {
        self.deviceSettings.fetchDeviceSettings()
        self.location.requestAuthorization()
        self.location.startUpdatingLocation()
    }
    
    private func resume() {
        
        guard let unwrapSettings = self.deviceItem, let unwrapLocation = self.locationItem else {return}
        self.delegate?.fetched(data: WindowDataEntity.make(deviceData: unwrapSettings, locationItem: unwrapLocation))
        self.locationItem = nil
    }
}

extension WindowDataInteractor : LocatorInteractorOutput {
    
    func fetched(status: AuthorizationStatus) {}
    
    func fetchedLocation(item: LocationItem) {
        self.locationItem = item
        self.resume()
    }
    
    func errorFetchingLocation() {}
    
    func errorFetchingAuthorization() {}
    
}

extension WindowDataInteractor : DeviceSettingsInteractorOutput {
    
    func fetchedGeneral(settings: DeviceSettingsItem) {
      self.deviceItem = settings
      self.resume()
    }
}
