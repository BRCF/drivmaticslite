//
//  WindowDataInteractorBuilder.swift
//  DrivmaticsLite
//
//  Created by Bruno on 16/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class WindowDataInteractorBuilder {
    
    static func make() -> WindowDataInteractorInput {
     
        let deviceInteractor = DeviceSettingsInteractorBuilder.make()
        
        let locationInteractor = LocatorInteractorBuilder.make()
        
        let window = WindowDataInteractor.init(deviceSettings: deviceInteractor, location: locationInteractor)
        
        
        deviceInteractor.output = window
        locationInteractor.output = window
        
        return window
    }
}
