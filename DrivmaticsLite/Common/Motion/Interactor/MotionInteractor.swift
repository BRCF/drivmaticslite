//
//  MotionInteractor.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 26/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol MotionInteractorInput {
    func startMotionUpdate()
    var delegate:MotionInteractorDelegate? {get set}
}

protocol MotionInteractorDelegate {
    func fetched(_ data:DeviceMotionItem)
}

final class MotionInteractor:MotionInteractorInput {
    
    var delegate:MotionInteractorDelegate?
    private let motionManager:MotionManagerInput
    
    init(motionManager:MotionManagerInput) {
        self.motionManager = motionManager
    }
    
    func startMotionUpdate() {
        
        if self.motionManager.isDeviceMotionAvailable() {
            self.motionManager.startDeviceMotionUpdates()
        }
    }
}

extension MotionInteractor: MotionManagerDelegate {
    
    func fetched(data: DeviceMotionItem) {
        self.delegate?.fetched(data)
    }
}



extension MotionInteractor {
    
    static func make() -> MotionInteractorInput {
        
        let manager = MotionManager.instance

        let motion = MotionInteractor.init(motionManager:manager)
        manager.delegate?.addDelegate(motion)
        
        return motion
    }
}
