//
//  AccelerometerManager.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 25/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import CoreMotion

protocol MotionManagerInput {
    func isDeviceMotionAvailable() -> Bool
    func startDeviceMotionUpdates()
    var delegate:MulticastDelegate<MotionManagerDelegate>? { get set }
}

protocol MotionManagerDelegate {
    func fetched(data:DeviceMotionItem)
}

final class MotionManager:MotionManagerInput {
    
    static let instance:MotionManagerInput = MotionManager()
    private let motionManager = CMMotionManager()
    private var timer:Timer?
    private var T:[CMGyroData?] = [CMGyroData?]()
    var delegate:MulticastDelegate<MotionManagerDelegate>? = MulticastDelegate<MotionManagerDelegate>()
    
    private init() {}
    
    func isDeviceMotionAvailable() -> Bool {
        return self.motionManager.isDeviceMotionAvailable
    }
    
    func testsDeviceMotion() {
        
        self.motionManager.gyroUpdateInterval = 0.01
        self.motionManager.startGyroUpdates(to: OperationQueue.main) { (data, error) in
            self.T.append(data)
        }
        
    }
    
    func startDeviceMotionUpdates() {
        
        self.motionManager.showsDeviceMovementDisplay = true
        self.motionManager.deviceMotionUpdateInterval = 0.01
        self.motionManager.startDeviceMotionUpdates(using: .xMagneticNorthZVertical)
        
        self.timer = Timer(fire: Date(), interval:0.01, repeats: true,
                           block: { (timer) in
                            if let data = self.motionManager.deviceMotion {
                                
                                self.delegate?.invoke({$0.fetched(data:SensorItemBuilder.makeWith(deviceMotion:data))})
                               
                            }
        })
        
        guard let unwrapTimer = self.timer else {return}
        
        RunLoop.current.add(unwrapTimer, forMode: RunLoop.Mode.default)
        
    }
}

