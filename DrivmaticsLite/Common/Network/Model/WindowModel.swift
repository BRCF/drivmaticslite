//
//  WindowModel.swift
//  DrivmaticsLite
//
//  Created by Bruno on 15/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

struct WindowModel:Codable {
    
    let id:Int?
    let vehicle:Int?
    let driver:Int?
    let dateTime:String?
    let nanoTime:Double?
    let deviceModel:String?
    let deviceVersion:String?
    let deviceSOVersion:String?
    let deviceSerial:String?
    let latitude:String?
    let longitude:String?
    let altitude:String?
    let accuracy:String?
    let speed:String?
    let sensors: [JSONAny]?
    
    enum CodingKeys:String, CodingKey {
        case id
        case vehicle
        case driver
        case dateTime = "datetime"
        case nanoTime = "nanotime"
        case deviceModel = "device_model"
        case deviceVersion = "device_version"
        case deviceSOVersion = "device_so_version"
        case deviceSerial = "device_serial"
        case latitude = "gps_lat"
        case longitude = "gps_long"
        case altitude = "gps_altitude"
        case accuracy = "gps_accuracy"
        case speed = "gps_speed"
        case sensors
    }
}
