//
//  TokenModel.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 24/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

struct TokenModel: Codable {
  
  let token: String
  
  enum CodingKeys: String, CodingKey {
    case token = "token"
  }
}
