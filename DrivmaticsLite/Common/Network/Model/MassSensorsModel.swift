//
//  MassSensorsModel.swift
//  DrivmaticsLite
//
//  Created by Bruno on 18/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

// MARK: - MassSensorsModel
struct MassSensorsModel: Codable {
    let id, vehicle, driver: Int
    let datetime: String
    let nanoTime: Int
    let deviceModel, deviceVersion, deviceSoVersion, deviceSerial: String
    let gpsLat, gpsLong, gpsAltitude, gpsAccuracy: String
    let gpsSpeed: String
    let sensors: [Sensor]
    
    enum CodingKeys: String, CodingKey {
        case id, vehicle, driver, datetime
        case nanoTime = "nano_time"
        case deviceModel = "device_model"
        case deviceVersion = "device_version"
        case deviceSoVersion = "device_so_version"
        case deviceSerial = "device_serial"
        case gpsLat = "gps_lat"
        case gpsLong = "gps_long"
        case gpsAltitude = "gps_altitude"
        case gpsAccuracy = "gps_accuracy"
        case gpsSpeed = "gps_speed"
        case sensors
    }
}

// MARK: - Sensor
struct Sensor: Codable {
    let id: Int
    let sensor, x, y, z: String
    let datetime: Date
    let nanoTime, windowData: Int
    
    enum CodingKeys: String, CodingKey {
        case id, sensor, x, y, z, datetime
        case nanoTime = "nano_time"
        case windowData = "window_data"
    }
}
