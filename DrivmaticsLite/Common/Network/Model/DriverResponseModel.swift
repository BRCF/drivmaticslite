//
//  DriverResponseModel.swift
//  DrivmaticsLite
//
//  Created by Bruno on 24/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

// MARK: - DriverResponseModel
struct DriverResponseModel: Codable {
    let id: Int?
    let deviceModel, deviceVersion, deviceSoVersion, deviceSerial: String?
    let gpsLat, gpsLong, gpsAltitude, gpsAccuracy: String?
    let gpsBearing, speed, accelerometerX, accelerometerY: String?
    let accelerometerZ, magnetometerX, magnetometerY, magnetometerZ: String?
    let magnetometerDegree, magnetometerElevation, gyroscopeX, gyroscopeY: String?
    let gyroscopeZ: String?
    let datetime: String?
    let vehicle, driver: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case deviceModel = "device_model"
        case deviceVersion = "device_version"
        case deviceSoVersion = "device_so_version"
        case deviceSerial = "device_serial"
        case gpsLat = "gps_lat"
        case gpsLong = "gps_long"
        case gpsAltitude = "gps_altitude"
        case gpsAccuracy = "gps_accuracy"
        case gpsBearing = "gps_bearing"
        case speed
        case accelerometerX = "accelerometer_x"
        case accelerometerY = "accelerometer_y"
        case accelerometerZ = "accelerometer_z"
        case magnetometerX = "magnetometer_x"
        case magnetometerY = "magnetometer_y"
        case magnetometerZ = "magnetometer_z"
        case magnetometerDegree = "magnetometer_degree"
        case magnetometerElevation = "magnetometer_elevation"
        case gyroscopeX = "gyroscope_x"
        case gyroscopeY = "gyroscope_y"
        case gyroscopeZ = "gyroscope_z"
        case datetime, vehicle, driver
    }
}
