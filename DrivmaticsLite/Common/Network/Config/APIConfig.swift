//
//  ApiConfig.swift
//
//  Created by Guilherme Siepmann on 16/11/18.
//  Copyright © 2018 TIM. All rights reserved.
//

import Foundation

enum APIConfig {
  
    static let baseApiUrl: String = "http://hml.drivmatics.com:8000/"
  
    enum Login {
        static let postLogin:String = "api-token-auth/"
    }
  
    enum driverList {
        static let driverList:String = "driver/driver"
    }
    
    enum DriverRouter {
        static let postDriverRouter:String = "driver/test_data/"
    }
    
    enum window_data {
        static let post:String = "driver/window_data/"
    }
    
    
    enum saveMassSensors {
        static let url = "driver/save_mass_of_sensors/"
    }
    
    enum Dependents {
        static let listDependentsURL: String = "customerGroupManagement/v1/customers"
        static let dependentActionURL: String = "customerGroupManagement/v1/customer"
        static let approvedDependentsURL: String = "usageConsumptionGroupManagement/v1/customer/"
        static let setUserInternetLimitURL: String = "customerBalanceGroupManagement/v1/customer/"
    }
    
    enum Faq {
        static let getFAQ = ""
    }
    
    enum AccountData {
        static let getAccountData = "v2/customer/accountData"
    }
    
    enum Products {
        static let getProducts = "v1/products?type=service&status=active,eligible"
        static let postProduct = "v1/products"
    }
    
    enum ChatBot {
        static let callChat = "api/auth"
    }
    
    enum AdditionalContracts {
        static let defaultIUrl = "434f15d633606edf0e29615f89cc2465/raw/1bef6891eb4a65721a4ce2ab908cf0055fb2d4d3/mock.json"
    }
}
