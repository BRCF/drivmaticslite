//
//  APIFactory.swift
//  Pods
//
//  Created by Leandro Silva on 24/03/17.
//
//

import Foundation

public struct APIFactory {
    
    public static let sharedInstance = APIFactory()
    
    private init() {
    }
    
    public var loginService:LoginService {
        return LoginService()
    }
    
    
    public var driverData:DriverDataService {
        return DriverDataService()
    }
    
    public var windowData:WindowDataService {
        return WindowDataService()
    }
    
}
