//
//  DriverDataService.swift
//  DrivmaticsLite
//
//  Created by Bruno on 24/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import Alamofire

private protocol DriverDataServiceProtocol {
    
    func postDriverDataService(router: DriverDataRouter, completion: @escaping (Result<DriverResponseModel>) -> Void)
}


public struct DriverDataService: DriverDataServiceProtocol, APIServiceConfiguration,APIErrorHandle {
    
    func postDriverDataService(router: DriverDataRouter, completion: @escaping (Result<DriverResponseModel>) -> Void) {
        
        
        self.manager.request(router).responseJSON { response -> Void in
            
            switch response.result {
            case .success(let value):
                do {
                    let response = try JSONDecoder().decode(DriverResponseModel.self, withJSONObject: value, options: [])
                    completion(Result.success(response))
                } catch {
                    completion(Result.failure(error))
                }
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}
