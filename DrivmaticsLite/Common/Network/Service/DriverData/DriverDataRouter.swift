//
//  DriverDataRouter.swift
//  DrivmaticsLite
//
//  Created by Bruno on 24/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import Alamofire


public class DriverEntity {
    
    let lat:String
    let long:String
    let speed:String
    let altitude:String
    let horizontalAccuracy:String
    let timestamp:String
    let accelerationX:String
    let accelerationY:String
    let accelerationZ:String
    let rotationRateX:String
    let rotationRateY:String
    let rotationRateZ:String
    let magneticFieldX:String
    let magneticFieldY:String
    let magneticFieldZ:String
    let graviyX:String
    let gravityY:String
    let gravityZ:String
    let deviceModel:String
    let systemVersion:String
    let deviceSerial:String
    
    init(lat:String, long:String, speed:String, timestamp:String, accelerationX:String, accelerationY:String, accelerationZ:String, rotationRateX:String, rotationRateY:String, rotationRateZ:String,
         magneticFieldX:String, magneticFieldY:String, magneticFieldZ:String, graviyX:String, gravityY:String, gravityZ:String, horizontalAccuracy:String, altitude:String, deviceModel:String, systemVersion:String, deviceSerial:String) {
        
        self.lat = lat
        self.long = long
        self.speed = speed
        self.altitude = altitude
        self.horizontalAccuracy = horizontalAccuracy
        self.timestamp = timestamp
        self.accelerationX = accelerationX
        self.accelerationY = accelerationY
        self.accelerationZ = accelerationZ
        self.rotationRateX = rotationRateX
        self.rotationRateY = rotationRateY
        self.rotationRateZ = rotationRateZ
        self.magneticFieldX = magneticFieldX
        self.magneticFieldY = magneticFieldY
        self.magneticFieldZ = magneticFieldZ
        self.deviceModel = deviceModel
        self.systemVersion = systemVersion
        self.deviceSerial = deviceSerial
        self.graviyX = graviyX
        self.gravityY = gravityY
        self.gravityZ = gravityZ
    }
}


public enum DriverDataRouter:APIRouter {
    
    case post(entity:DriverEntity)
    
    var path: String {
        return APIConfig.DriverRouter.postDriverRouter
    }
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .post(_):
            return .post
        }
    }
    
    
    
    var httpBody: Body {
        switch self {
        case  .post(let arg):
            
            return  [
                "device_model" : arg.deviceModel,
                "device_version" : "-",
                "device_so_version" : arg.systemVersion,
                "device_serial" : arg.deviceSerial,
                "gps_lat" : arg.lat,
                "gps_long" : arg.long,
                "gps_altitude" : arg.altitude,
                "gps_accuracy": arg.horizontalAccuracy,
                "gps_bearing" : "-",
                "speed" : arg.speed,
                "accelerometer_x" : arg.accelerationX,
                "accelerometer_y" : arg.accelerationY,
                "accelerometer_z" : arg.accelerationZ,
                "magnetometer_x" : arg.magneticFieldX,
                "magnetometer_y" : arg.magneticFieldY,
                "magnetometer_z" : arg.magneticFieldZ,
                "magnetometer_degree" : "-",
                "magnetometer_elevation" : "-",
                "datetime" : arg.timestamp,
                "vehicle" : 1,
                "driver" : 1,
                "gyroscope_x" : arg.rotationRateX,
                "gyroscope_y" : arg.rotationRateY,
                "gyroscope_z" : arg.rotationRateZ,
                "device_motion_x" : arg.graviyX,
                "device_motion_y" : arg.gravityY,
                "device_motion_z" : arg.gravityZ
            ]
        }
    }
}
