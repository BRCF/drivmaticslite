//
//  WindowDataRouter.swift
//  DrivmaticsLite
//
//  Created by Bruno on 15/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import Alamofire

public enum WindowDataRouter:APIRouter {
    
    case post(entity:WindowDataEntity)
    
    var path: String {
        return APIConfig.window_data.post
    }
    
    var method: Alamofire.HTTPMethod {
        return .post
    }
    
    var httpBody: Body {
        
        switch self {
        case .post(let arg):
            return [
                "vehicle" : arg.vehicle,
                "driver" : arg.driver,
                "datetime" : arg.timestamp,
                "nano_time" : arg.nanotime,
                "device_model" : arg.deviceModel,
                "device_version" : "-",
                "device_so_version" : arg.deviceSOVersion,
                "device_serial" : arg.deviceSerial,
                "gps_lat" : arg.latitude,
                "gps_long" : arg.longitude,
                "gps_altitude" : arg.latitude,
                "gps_accuracy" : arg.horizontalAccuracy,
                "gps_speed" : arg.speed
            ]
        }
    }
}
