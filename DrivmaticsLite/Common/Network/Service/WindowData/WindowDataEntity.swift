//
//  WindowDataModel.swift
//  DrivmaticsLite
//
//  Created by Bruno on 15/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

public class WindowDataEntity {
    
    let vehicle:Int
    let driver:Int
    let timestamp:String
    let nanotime:Int64
    let deviceModel:String
    let deviceSOVersion:String
    let deviceSerial:String
    let latitude:String
    let longitude:String
    let altitude:String
    let horizontalAccuracy:String
    let speed:String
    
    init(vehicle:Int, driver:Int, timestamp:String, nanotime:Int64, deviceModel:String, deviceSOVersion:String, deviceSerial:String, latitude:String, longitude:String, altitude:String, horizontalAccuracy:String, speed:String) {
        
        self.vehicle = vehicle
        self.driver = driver
        self.timestamp = timestamp
        self.nanotime = nanotime
        self.deviceModel = deviceModel
        self.deviceSOVersion = deviceSOVersion
        self.deviceSerial = deviceSerial
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.horizontalAccuracy = horizontalAccuracy
        self.speed = speed
        
    }
    
    
    static func make(deviceData:DeviceSettingsItem, locationItem:LocationItem) -> WindowDataEntity {

        let windowEntity = WindowDataEntity.init(
            vehicle: 1,
            driver: 1,
            timestamp: DateFormat.getDateFromTimeStamp(timeStamp:locationItem.timestamp),
            nanotime: locationItem.timestamp.millisecondsSince1970,
            deviceModel: deviceData.deviceModel,
            deviceSOVersion: deviceData.systemVersion,
            deviceSerial: deviceData.uuid,
            latitude:String(locationItem.latitude),
            longitude: String(locationItem.longitude),
            altitude: String(locationItem.altitude) ,
            horizontalAccuracy: String(locationItem.horizontalAccuracy),
            speed: String(locationItem.speed)
        )
        
        return windowEntity

    }
}
