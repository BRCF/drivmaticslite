//
//  WindowDataService.swift
//  DrivmaticsLite
//
//  Created by Bruno on 15/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import Alamofire

private protocol WindowDataServiceProtocol {
    func post(router:WindowDataRouter, completion:@escaping(Result<WindowModel>) -> Void)
}


public struct WindowDataService:WindowDataServiceProtocol,APIServiceConfiguration,APIErrorHandle {
    
    func post(router: WindowDataRouter, completion: @escaping (Result<WindowModel>) -> Void) {
        
        self.manager.request(router).responseJSON { (data) in
            
            switch data.result {
                
            case .success(let value):
                do {
                    
                    print("GPS -> \(data.result.isSuccess)")
                    
                    let response = try JSONDecoder().decode(WindowModel.self, withJSONObject: value, options: [])
                    
                    completion(Result.success(response))
                    
                } catch {
                    completion(Result.failure(error))
                }
                
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}
