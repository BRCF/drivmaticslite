//
//  LoginService.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 24/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import Alamofire

private protocol LoginServiceProtocol {
  
  func postProduct(router: LoginRouter, completion: @escaping (Result<TokenModel>) -> Void)
}



public struct LoginService: LoginServiceProtocol, APIServiceConfiguration,APIErrorHandle {
  
  
  func postProduct(router: LoginRouter, completion: @escaping (Result<TokenModel>) -> Void) {
    
    self.manager.request(router).responseJSON { response -> Void in
      
      
      switch response.result {
      case .success(let value):
        do {
          let response = try JSONDecoder().decode(TokenModel.self, withJSONObject: value, options: [])
            
          completion(Result.success(response))
        } catch {
          completion(Result.failure(error))
        }
      case .failure(let error):
        completion(Result.failure(error))
      }
    }
  }
}
