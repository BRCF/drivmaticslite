//
//  LoginRouter.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 24/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import Alamofire

public enum LoginRouter:APIRouter {

  case post(username:String, pass:String)
  
  var path: String {
    return APIConfig.Login.postLogin
  }
  
  
  var method: Alamofire.HTTPMethod {
    switch self {
    case .post(_,_):
      return .post
    }
  }
  
  var httpBody: Body {
    switch self {
    case .post(let username,let pass):
      return ["username":username, "password":pass]
    }
  }
  
}
