//
//  MassSensorsRouter.swift
//  DrivmaticsLite
//
//  Created by Bruno on 18/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import Alamofire

public enum MassSensorsRouter:APIRouter {
    
    case post(T:MassSensorsEntity)
    
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .post(_):
            return .post
        }
    }
    
    var path: String {
        return APIConfig.saveMassSensors.url
    }
    
    
    func make(_ arg:MassSensorsEntity) -> Any {
        
        func convert(_ data:SensorEntity) -> Any {
            return [
                "sensor": data.sensor,
                "x": data.x,
                "y": data.y,
                "z": data.z,
                "datetime": data.dateTime,
                "nano_time": data.nanoTime,
                "window_data": data.id
            ]
        }
        
        let T = arg.sensors.map({convert($0)})
        
       return T
    }
    
    var httpBody: Body {
        
        switch self {
        case .post(let arg):
            return [
                "window_data":arg.id,
                "sensors" : self.make(arg)
            ]
        }
    }
}
