//
//  MassSensorsService.swift
//  DrivmaticsLite
//
//  Created by Bruno on 21/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import Alamofire

private protocol MassSensorsServiceProtocol {
    func post(router:MassSensorsRouter, completion:@escaping(Result<MassSensorsModel>) -> Void)
}

public struct MassSensorsService:MassSensorsServiceProtocol, APIServiceConfiguration, APIErrorHandle {
    
    func post(router: MassSensorsRouter, completion: @escaping (Result<MassSensorsModel>) -> Void) {
     
        
        
        self.manager.request(router).responseJSON { (data) in
            
            switch data.result {
                
            case .success(let value):
                do {
                    
                    print("GPS -> \(data.result.isSuccess)")
                    
                    let response = try JSONDecoder().decode(MassSensorsModel.self, withJSONObject: value, options: [])
                    
                    completion(Result.success(response))
                    
                } catch {
                    completion(Result.failure(error))
                }
                
            case .failure(let error):
                completion(Result.failure(error))
            }
            
        }
        
    }
}
