
//
//  MassSensorsEntity.swift
//  DrivmaticsLite
//
//  Created by Bruno on 20/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

public struct MassSensorsEntity {
    
    let id:Int
    let sensors:[SensorEntity]
    
    init(id:Int, sensors:[SensorEntity]) {
        self.id = id
        self.sensors = sensors
    }
    
}

public struct SensorEntity {
    
    let sensor:String
    let x:String
    let y:String
    let z:String
    let dateTime:String
    let nanoTime:Int64
    let id:Int
    
    init(sensor:String, x:String, y:String, z:String, dateTime:String, nanoTime:Int64, id:Int) {
        self.sensor = sensor
        self.x = x
        self.y = y
        self.z = z
        self.dateTime = dateTime
        self.nanoTime = nanoTime
        self.id = id
    }
    
}
