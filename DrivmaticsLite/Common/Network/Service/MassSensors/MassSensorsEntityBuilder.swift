//
//  MassSensorsEntityBuilder.swift
//  DrivmaticsLite
//
//  Created by Bruno on 21/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class MassSensorsEntityBuilder {
    
    static func make(_ id:Int, _ sensorItem:[SensorItem]) -> MassSensorsEntity {
        
        let arr = sensorItem.map({MassSensorsEntityBuilder.convert($0, id: id)})
        
        return MassSensorsEntity.init(id:id, sensors:arr)
    
    }
    
    private static func convert(_ arg:SensorItem, id:Int) -> SensorEntity {
        
        return SensorEntity.init(sensor: arg.type.rawValue,
                                             x: String(arg.x),
                                             y: String(arg.x),
                                             z: String(arg.z),
                                             dateTime:DateFormat.getDateFromTimeStamp(timeStamp:arg.date),
                                             nanoTime:arg.date.millisecondsSince1970,
                                             id:id)
    }
}

