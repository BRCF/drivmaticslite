//
//  DispatchViewController.swift
//  Drivmatics
//
//  Created by Bruno Raphael Castilho de Freitas on 11/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import UIKit

class Dispatch {
 
    private var window:UIWindow?
    
    func startAppIn(_ window:UIWindow){
        self.window = window
      
      if NSUserDefaultsHelper.getLoginToken() != nil {
        window.rootViewController = GPSScreenWireframe().make()
      } else {
        window.rootViewController = StartScreenWireframe().make()
      }
      
        window.makeKeyAndVisible()
    }
    
    func isIdleTimer(_ disable:Bool){
        UIApplication.shared.isIdleTimerDisabled = disable
    }
    
}
