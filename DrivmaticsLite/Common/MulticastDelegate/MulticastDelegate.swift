//
//  MulticastDelegate.swift
//  GloboLive
//
//  Created by Wellington Chaves on 26/03/19.
//  Copyright © 2019 Globo Comunicação e Participações S.A. All rights reserved.
//

import Foundation

final class MulticastDelegate <T> {
    
    private var weakDelegates = NSHashTable<AnyObject>.weakObjects()
    
    func addDelegate(_ delegate: T) {
        weakDelegates.add(delegate as AnyObject)
    }
    
    func removeDelegate(_ delegate: T) {
        weakDelegates.remove(delegate as AnyObject)
    }
    
    func count() -> Int {
        return weakDelegates.count
    }
    
    func invoke(_ invocation: (T) -> ()) {
        
        for delegate in weakDelegates.allObjects {
            invocation(delegate as! T)
        }
    }
}

func += <T: AnyObject> (left: MulticastDelegate<T>, right: T) {
    left.addDelegate(right)
}

func -= <T: AnyObject> (left: MulticastDelegate<T>, right: T) {
    left.removeDelegate(right)
}
