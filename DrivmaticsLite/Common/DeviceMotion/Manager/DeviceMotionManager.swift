//
//  DeviceMotionManager.swift
//  DrivmaticsLite
//
//  Created by Bruno on 21/06/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import CoreMotion

final class DeviceMotionManager {
    
    let deviceMotion = CMDeviceMotion()
    
    func startAccelerometerUpdates() {
        
        deviceMotion.userAccelerationInReferenceFrame
        
    }
}
