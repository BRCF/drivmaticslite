//
//  DeviceSettingsInteractorBuilder.swift
//  GloboLive
//
//  Created by Bruno Castilho on 3/20/19

import Foundation

final class DeviceSettingsInteractorBuilder {
    
    static func make() -> DeviceSettingsInteractor {
        
        let interactor = DeviceSettingsInteractor()
        return interactor
        
    }
}
