//
//  DeviceSettingsInteractor.swift
//  GloboLive
//
//  Created by Bruno Castilho on 3/19/19.

import UIKit

protocol DeviceSettingsInteractorInput {
    
    func fetchDeviceSettings()
}


protocol DeviceSettingsInteractorOutput: class {
    
    func fetchedGeneral(settings:DeviceSettingsItem)
}


final class DeviceSettingsInteractor : DeviceSettingsInteractorInput {
    
    weak var output:DeviceSettingsInteractorOutput?
   
    func fetchDeviceSettings() {
          self.output?.fetchedGeneral(settings: self.makeEntity())
    }
}

extension DeviceSettingsInteractor {
    
    
    private func makeEntity() -> DeviceSettingsItem {
        
        var item:DeviceSettingsItem =  DeviceSettingsItem()
        
        item.systemVersion = UIDevice.SOVersion()
        item.deviceModel = UIDevice.modelName
        item.uuid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        item.screenResolution = "\(String.init(format: "%.0f", UIScreen.main.bounds.size.width)) X \(String.init(format: "%.0f", UIScreen.main.bounds.size.height))"
        
        if let unwrapVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            
            item.appVersion = unwrapVersion
        }
        
        return item        
    }
}
