//
//  DeviceSettingsItem.swift
//  GloboLive
//
//  Created by Bruno Castilho on 3/21/19.
//  Copyright © 2019 Globo Comunicação e Participações S.A. All rights reserved.
//

import Foundation
import CoreGraphics

struct DeviceSettingsItem {
    
    var addressIP:String = ""
    var appVersion:String = ""
    var deviceModel:String = ""
    var systemVersion:String = ""
    var uuid:String = ""
    var screenResolution:String = ""
    var manufactirer:String = "Apple"
}
