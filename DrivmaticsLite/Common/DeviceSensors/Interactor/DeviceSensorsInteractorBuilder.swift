//
//  DeviceSensorsInteractorBuilder.swift
//  DrivmaticsLite
//
//  Created by Bruno on 16/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class DeviceSensorsInteractorBuilder {
    
    static func make() -> DeviceSensorsInteractor {
        
        var gatewayWindowDataInteractor = GatewayWindowDataInteractorBuilder.make()
        var globalSystemInteractor = GlobalSystemInteractorBuilder.make()
        
        let deviceSensorsInteractor = DeviceSensorsInteractor.init(window: gatewayWindowDataInteractor, globalSystemInteractor:globalSystemInteractor)
        
        gatewayWindowDataInteractor.delegate = deviceSensorsInteractor
        globalSystemInteractor.output = deviceSensorsInteractor
        
        return deviceSensorsInteractor
    }

}
