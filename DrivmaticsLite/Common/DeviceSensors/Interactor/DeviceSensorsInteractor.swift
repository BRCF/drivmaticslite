//
//  DeviceSensorsInteractor.swift
//  DrivmaticsLite
//
//  Created by Bruno on 16/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol DeviceSensorsInteractorInput {
    func startWindow()
    var window:GatewayWindowDataInteractorInput { get }
    var delegate:DeviceSensorsInteractorDelegate? { get }
}

protocol DeviceSensorsInteractorDelegate {
    func fetched(GPS:WindowType)
}

final class DeviceSensorsInteractor:DeviceSensorsInteractorInput {
    
    var delegate:DeviceSensorsInteractorDelegate?
    var window:GatewayWindowDataInteractorInput
    var globalSystemInteractor:GlobalSystemInteractorInput
    
    init(window:GatewayWindowDataInteractorInput, globalSystemInteractor:GlobalSystemInteractorInput) {
        self.window = window
        self.globalSystemInteractor = globalSystemInteractor
    }
    
    func startWindow() {
        self.globalSystemInteractor.start()
        self.window.turnOn()
    }
}

extension DeviceSensorsInteractor : GatewayWindowDataInteractorOutput {
    
    func updated(value: Int) {
        self.globalSystemInteractor.stop(value)
    }
    
    func fetched(_ type: WindowType) {
        self.delegate?.fetched(GPS:type)
    }
}

extension DeviceSensorsInteractor : GlobalSystemInteractorOutput {
    
    func fetched(data: [SensorItem], _ id: Int) {
        
    }
}
