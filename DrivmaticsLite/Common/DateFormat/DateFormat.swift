//
//  DateFormat.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 25/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class DateFormat {
  
  static func getDateFromTimeStamp(timeStamp:Date) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return dateFormatter.string(from: timeStamp)
    
  }
}


