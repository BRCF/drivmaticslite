//
//  UserDefaultsHelper.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 24/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

open class NSUserDefaultsHelper {
  
  static func saveLoginToken(_ token:String) {
    let defaults = UserDefaults.standard
    defaults.set(token, forKey: "token")
  }
  
  static func getLoginToken() -> String? {
    let defaults = UserDefaults.standard
    return defaults.string(forKey: "token")
  }
  
}
