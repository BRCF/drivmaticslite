//
//  TrackEventInteractor.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 26/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol TrackEventInteractorInput {
    func requestGPSAuthorization()
    func startGPS()
    func startSensors()
    func stopSensors()
}

protocol TrackEventInteractorOutput {
    func trackEvent(data:TrackEventItem)
}


class TrackEventInteractor:TrackEventInteractorInput {
    
    let locatorInteractor:LocatorInteractor
    let motionInteractor:MotionInteractorInput
    let deviceInteractor:DeviceSettingsInteractor
    var delegate:TrackEventInteractorOutput?
    var trackItem:TrackEventItem = TrackEventItem()
    
    init(locatorInteractor:LocatorInteractor,motionInteractor:MotionInteractorInput, deviceInteractor:DeviceSettingsInteractor) {
        self.locatorInteractor = locatorInteractor
        self.motionInteractor = motionInteractor
        self.deviceInteractor = deviceInteractor
    }
    
    func requestGPSAuthorization() {
        self.locatorInteractor.requestAuthorization()
    }
    func startSensors() {
        
    }
    
    func stopSensors() {
        
    }
    
    func startGPS() {
        self.locatorInteractor.startUpdatingLocation()
        self.deviceInteractor.fetchDeviceSettings()
    }

    func stopSensorsNow() {
        self.locatorInteractor.stopUpdating()
    }
}

#warning("REMOVER #7")
//extension TrackEventInteractor {
//    
//    static func make() -> TrackEventInteractor {
//        
//        let lotacorInteractor = LocatorInteractorBuilder.make()
//        let motionInteractor = MotionInteractor.init()
//        let deveceSettingsInteractor = DeviceSettingsInteractor.init()
//        
//        let myInstance = TrackEventInteractor.init(locatorInteractor:lotacorInteractor,  motionInteractor: motionInteractor, deviceInteractor: deveceSettingsInteractor)
//        
//        lotacorInteractor.output = myInstance
//        motionInteractor.delegate = myInstance
//        deveceSettingsInteractor.output = myInstance
//        
//        return myInstance
//    }
//    
//}

extension TrackEventInteractor : DeviceSettingsInteractorOutput {
    func fetchedGeneral(settings: DeviceSettingsItem) {
        self.trackItem.deviceModel = settings.deviceModel
        self.trackItem.deviceSerial = settings.uuid
        self.trackItem.systemVersion = settings.systemVersion
    }
}

//extension TrackEventInteractor:MotionInteractorOuput {
//
//    func MotionGravityUpdate(x: Double, y: Double, z: Double) {
//        self.trackItem.grativyX = x
//        self.trackItem.gravityY = y
//        self.trackItem.gravityZ = z
//    }
//
//
//    func accelerometerUpdates(x: Double, y: Double, z: Double) {
//        self.trackItem.accelerationX = x
//        self.trackItem.accelerationY = y
//        self.trackItem.accelerationZ = z
//    }
//
//    func gyroUpdate(x: Double, y: Double, z: Double) {
//        self.trackItem.rotationRateX = x
//        self.trackItem.rotationRateY = y
//        self.trackItem.rotationRateZ = z
//    }
//
//    func magnetometerUpdate(x: Double, y: Double, z: Double) {
//        self.trackItem.magneticFieldX = x
//        self.trackItem.magneticFieldY = y
//        self.trackItem.magneticFieldZ = z
//    }
//}

extension TrackEventInteractor:LocatorInteractorOutput {
    
    func fetched(status: AuthorizationStatus) {}
    
    func fetchedLocation(item: LocationItem) {
        
        self.trackItem.latitude = item.latitude
        self.trackItem.longitude = item.longitude
        self.trackItem.speed = item.speed
        self.trackItem.timestamp = item.timestamp
        self.trackItem.altitude = item.altitude
        self.trackItem.horizontalAccuracy = item.horizontalAccuracy
        
        
        self.delegate?.trackEvent(data: self.trackItem)
        
    }
    
    func errorFetchingLocation() {}
    
    func errorFetchingAuthorization() {}
    
}


class TrackEventItem {
    
    var latitude:Double = 0
    var longitude:Double = 0
    var altitude:Double = 0.0
    var horizontalAccuracy:Double = 0.0
    var speed:Double = 0.0
    var timestamp:Date = Date()
    var accelerationX:Double = 0.00000
    var accelerationY:Double = 0.00000
    var accelerationZ:Double = 0.00000
    var rotationRateX:Double = 0.00000
    var rotationRateY:Double = 0.00000
    var rotationRateZ:Double = 0.00000
    var magneticFieldX:Double = 0.00000
    var magneticFieldY:Double = 0.00000
    var magneticFieldZ:Double = 0.00000
    var grativyX:Double = 0.00000
    var gravityY:Double = 0.00000
    var gravityZ:Double = 0.00000
    var deviceModel:String = "-"
    var systemVersion:String = "-"
    var deviceSerial:String = "-"
    
}


