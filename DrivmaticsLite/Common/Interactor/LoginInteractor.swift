//
//  LoginInteractor.swift
//  DrivmaticsLite
//
//  Created by Bruno Raphael Castilho de Freitas on 24/05/2019.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol LoginInteractorInput {
  func processLogin(name:String, pass:String, complete:@escaping (String?)->Void)
}

final class LoginInteractor:LoginInteractorInput {
  
  func processLogin(name:String, pass:String, complete:@escaping (String?)->Void) {
    
    let service = APIFactory.sharedInstance.loginService
    let router = LoginRouter.post(username: name, pass: pass)
    
    service.postProduct(router: router) { (result: Result<TokenModel>)  in
      
      if result.isSuccess, let value = result.value {
        NSUserDefaultsHelper.saveLoginToken(value.token)
        complete(value.token)
      }else {
        complete(nil)
      }
    }
  }
  
}
