//
//  DriverDataInteractor.swift
//  DrivmaticsLite
//
//  Created by Bruno on 24/05/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol DriverDataInteractorInput {
    func processDriverData(lat: Double, long: Double, speed: Double, timestamp: Date, accelerationX: Double, accelerationY: Double, accelerationZ: Double, rotationRateX: Double, rotationRateY: Double, rotationRateZ: Double, magneticFieldX: Double, magneticFieldY: Double, magneticFieldZ: Double, gravityX:Double, gravityY:Double, gravityZ:Double, altitude: Double, horizontalAccuracy: Double,deviceModel:String, systemVersion:String, deviceSerial:String, complete: @escaping (Bool) -> Void)
}

final class DriverDataInteractor:DriverDataInteractorInput {
   
    func processDriverData(lat: Double, long: Double, speed: Double, timestamp: Date, accelerationX: Double, accelerationY: Double, accelerationZ: Double, rotationRateX: Double, rotationRateY: Double, rotationRateZ: Double, magneticFieldX: Double, magneticFieldY: Double, magneticFieldZ: Double, gravityX:Double, gravityY:Double, gravityZ:Double, altitude: Double, horizontalAccuracy: Double,deviceModel:String, systemVersion:String, deviceSerial:String, complete: @escaping (Bool) -> Void) {
    
    let service = APIFactory.sharedInstance.driverData
    let router = DriverDataRouter.post(entity: DriverEntity.init(lat: String(lat), long: String(long), speed: String(speed), timestamp: DateFormat.getDateFromTimeStamp(timeStamp: timestamp), accelerationX: String(accelerationX), accelerationY: String(accelerationY), accelerationZ: String(accelerationZ), rotationRateX: String(rotationRateX), rotationRateY: String(rotationRateY), rotationRateZ: String(rotationRateZ), magneticFieldX: String(magneticFieldX), magneticFieldY: String(magneticFieldY), magneticFieldZ: String(magneticFieldZ), graviyX: String(gravityX), gravityY:String(gravityY), gravityZ:String(gravityZ), horizontalAccuracy: String(horizontalAccuracy), altitude: String(altitude), deviceModel:deviceModel, systemVersion: systemVersion, deviceSerial: deviceSerial))
    
    //"2019-05-22 21:12:00"
    
    service.postDriverDataService(router: router) { (result : Result<DriverResponseModel>) in
      complete(result.isSuccess)
    }
  }
}



