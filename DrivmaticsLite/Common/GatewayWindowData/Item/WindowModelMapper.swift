//
//  WindowModelMapper.swift
//  DrivmaticsLite
//
//  Created by Bruno on 16/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

struct WindowType {
    let id:Int?
    let latitude:String?
    let longitude:String?
    let speed:String?
}

final class WindowModelMapper {
    static func make(_ model:WindowModel) -> WindowType {
        return WindowType.init(id: model.id, latitude: model.latitude, longitude: model.longitude, speed: model.speed)
    }
}


