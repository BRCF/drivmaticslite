//
//  GatewayWindowDataInteractor.swift
//  DrivmaticsLite
//
//  Created by Bruno on 16/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol GatewayWindowDataInteractorInput {
    func turnOn()
    var windowInteractor:WindowDataInteractorInput {get}
    var delegate:GatewayWindowDataInteractorOutput? {get set}
}

protocol GatewayWindowDataInteractorOutput {
    func updated(value:Int)
    func fetched(_ type:WindowType)
}

final class GatewayWindowDataInteractor : GatewayWindowDataInteractorInput {
    
    var windowInteractor:WindowDataInteractorInput
    var delegate:GatewayWindowDataInteractorOutput?
    
    init(windowInteractor:WindowDataInteractorInput) {
        self.windowInteractor = windowInteractor
    }
    
    func turnOn() {
        self.windowInteractor.start()
    }
    
    private func push(_ data: WindowDataEntity) {
        
        let service = APIFactory.sharedInstance.windowData
        let router = WindowDataRouter.post(entity: data)
        
        service.post(router: router) { (T) in
            
            if let data = T.value {
                
                if let unwrapID = data.id {
                    self.delegate?.updated(value: unwrapID)
                }
                
                self.delegate?.fetched(WindowModelMapper.make(data))
            }
        }
    }
}


extension GatewayWindowDataInteractor : WindowDataInteractorDelegate {
    
    func fetched(data: WindowDataEntity) {
        self.push(data)
    }
}
