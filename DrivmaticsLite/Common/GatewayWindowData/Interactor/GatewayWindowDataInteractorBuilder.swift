//
//  GatewayWindowDataInteractorBuilder.swift
//  DrivmaticsLite
//
//  Created by Bruno on 16/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class GatewayWindowDataInteractorBuilder {
    
    static func make() ->  GatewayWindowDataInteractorInput {
        
        var windowInteractor = WindowDataInteractorBuilder.make()

        let gatewayInteractor = GatewayWindowDataInteractor.init(windowInteractor:windowInteractor)
        
        windowInteractor.delegate = gatewayInteractor
        
        return gatewayInteractor
        
    }
}
