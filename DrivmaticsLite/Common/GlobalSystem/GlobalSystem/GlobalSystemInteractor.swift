//
//  GlobalSystemInteractor.swift
//  DrivmaticsLite
//
//  Created by Bruno on 27/06/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

protocol GlobalSystemInteractorInput {
    func start()
    func stop(_ id:Int)
    var output:GlobalSystemInteractorOutput? { get set }
}

protocol GlobalSystemInteractorOutput {
    func fetched(data:[SensorItem], _ id:Int)
}

final class GlobalSystemInteractor:GlobalSystemInteractorInput {
    
    private let motionInteractor:MotionInteractorInput
    private var normalizedSensors:[SensorItem] = []
    var output:GlobalSystemInteractorOutput?
    
    init(motionInteractor:MotionInteractorInput) {
        self.motionInteractor = motionInteractor
    }
    
    func start() {
        self.motionInteractor.startMotionUpdate()
    }
    
    func stop(_ id:Int) {
        self.pull(data: self.normalizedSensors, id: id)
        self.normalizedSensors.removeAll()
    }
    
    private func pull(data:[SensorItem], id:Int) {
        
        
        DispatchQueue.global(qos: .utility).async {
            
            let entity = MassSensorsEntityBuilder.make(id, data)
            let router = MassSensorsRouter.post(T: entity)
            let service = MassSensorsService.init()
            
            service.post(router: router) { (data) in }
        }
    }
}

extension GlobalSystemInteractor:MotionInteractorDelegate {
    
    func fetched(_ data: DeviceMotionItem) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            GlobalSystemConversor.convertWith(item: data.acceleration, matrix: data.attitudeRotationMatrix) {(convertedData) in
                self.normalizedSensors.append(convertedData)
            }
            
            GlobalSystemConversor.convertWith(item: data.rotation, matrix: data.attitudeRotationMatrix) { (convertedData) in
                self.normalizedSensors.append(convertedData)
            }
            
            GlobalSystemConversor.convertWith(item: data.magneticField, matrix: data.attitudeRotationMatrix) { (data) in
                self.normalizedSensors.append(data)
            }
        }
    }
}


