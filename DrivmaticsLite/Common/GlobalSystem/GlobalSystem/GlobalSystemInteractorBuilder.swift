//
//  GlobalSystemInteractorBuilder.swift
//  DrivmaticsLite
//
//  Created by Bruno on 18/07/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

final class GlobalSystemInteractorBuilder {
    
    static func make() -> GlobalSystemInteractorInput{
        
        var motionInteractor = MotionInteractor.make()
        let globalSysteminteractor = GlobalSystemInteractor.init(motionInteractor:motionInteractor)
        
        motionInteractor.delegate = globalSysteminteractor
        
        return globalSysteminteractor
        
    }
}
