//
//  GlobalSystemManager.swift
//  DrivmaticsLite
//
//  Created by Bruno on 27/06/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation


protocol GlobalSystemConversorInterface {
    static func  convertWith(item:SensorItem, matrix:RotationMatrixItem,completion:@escaping (SensorItem)->Void)
}

final class GlobalSystemConversor:GlobalSystemConversorInterface {
 
    static func convertWith(item:SensorItem, matrix:RotationMatrixItem,completion:@escaping (SensorItem)->Void) {
        
   //     DispatchQueue.global(qos: .userInteractive).async {
            
            let _x:Double = item.x*matrix.m11 + item.y*matrix.m12 + item.z*matrix.m13
            let _y:Double = item.x*matrix.m21 + item.y*matrix.m22 + item.z*matrix.m23
            let _z:Double = item.x*matrix.m31 + item.y*matrix.m32 + item.z*matrix.m33
            
            completion(SensorItem.init(x: _x, y: _y, z: _z, timestamp: item.timestamp, type: item.type))
        //}
    }
}
