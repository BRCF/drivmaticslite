//
//  NormalizedItem.swift
//  DrivmaticsLite
//
//  Created by Bruno on 27/06/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

struct NormalizedItem {
    
    var normalizedAcceleration:[SensorItem] = []
    var normalizedRotation:[SensorItem] = []
    var location:LocationItem?
    
}
