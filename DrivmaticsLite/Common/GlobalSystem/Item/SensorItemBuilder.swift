//
//  File.swift
//  DrivmaticsLite
//
//  Created by Bruno on 27/06/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation
import CoreMotion

final class SensorItemBuilder {
    
    static func makeWith(deviceMotion:CMDeviceMotion) -> DeviceMotionItem {
        
        let accelerationItem:SensorItem = SensorItem.init(x: deviceMotion.userAcceleration.x, y: deviceMotion.userAcceleration.y, z: deviceMotion.userAcceleration.z, timestamp: deviceMotion.timestamp, type: .acceleration)
      
        let rotationItem:SensorItem = SensorItem.init(x: deviceMotion.rotationRate.x, y: deviceMotion.rotationRate.y, z: deviceMotion.rotationRate.z, timestamp: deviceMotion.timestamp, type: .rotation)
        
        let magneticFieldItem:SensorItem = SensorItem.init(x: deviceMotion.magneticField.field.x, y:deviceMotion.magneticField.field.y , z: deviceMotion.magneticField.field.z, timestamp: deviceMotion.timestamp, type: .magneticField)
        
        let matrix:RotationMatrixItem = RotationMatrixItem.init(m11: deviceMotion.attitude.rotationMatrix.m11,
                                                                m12: deviceMotion.attitude.rotationMatrix.m12,
                                                                m13: deviceMotion.attitude.rotationMatrix.m13,
                                                                m21: deviceMotion.attitude.rotationMatrix.m21,
                                                                m22: deviceMotion.attitude.rotationMatrix.m22,
                                                                m23: deviceMotion.attitude.rotationMatrix.m23,
                                                                m31: deviceMotion.attitude.rotationMatrix.m31,
                                                                m32: deviceMotion.attitude.rotationMatrix.m32,
                                                                m33: deviceMotion.attitude.rotationMatrix.m33)
        
        
        return DeviceMotionItem.init(acceleration: accelerationItem, rotation: rotationItem, magneticField: magneticFieldItem, attitudeRotationMatrix: matrix)
        
    }
}
