//
//  GlobalSystemItem.swift
//  DrivmaticsLite
//
//  Created by Bruno on 27/06/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import Foundation

enum SensorType:String {
    case acceleration = "accelerometer"
    case rotation = "gyroscope"
    case magneticField = "magnetometer"
}

public struct SensorItem {
    
    var x: Double
    var y: Double
    var z: Double
    let timestamp:TimeInterval
    let type:SensorType
    let date:Date = Date()
    
     init(x: Double, y: Double, z: Double, timestamp:TimeInterval, type:SensorType) {
        self.x = x
        self.y = y
        self.z = z
        self.type = type
        self.timestamp = timestamp
    }
}

public struct RotationMatrixItem {
    
    var m11: Double
    var m12: Double
    var m13: Double
    var m21: Double
    var m22: Double
    var m23: Double
    var m31: Double
    var m32: Double
    var m33: Double
    
    public init(m11: Double, m12: Double, m13: Double, m21: Double, m22: Double, m23: Double, m31: Double, m32: Double, m33: Double) {
        self.m11 = m11
        self.m12 = m12
        self.m13 = m13
        self.m21 = m21
        self.m22 = m22
        self.m23 = m23
        self.m31 = m31
        self.m32 = m32
        self.m33 = m33
    }
}


public struct DeviceMotionItem {
    
    var acceleration:SensorItem
    var rotation:SensorItem
    var magneticField:SensorItem
    var attitudeRotationMatrix:RotationMatrixItem
    
    
    public init (acceleration:SensorItem, rotation:SensorItem, magneticField:SensorItem, attitudeRotationMatrix:RotationMatrixItem) {
        
        self.acceleration = acceleration
        self.rotation = rotation
        self.magneticField = magneticField
        self.attitudeRotationMatrix = attitudeRotationMatrix
        
    }
}



