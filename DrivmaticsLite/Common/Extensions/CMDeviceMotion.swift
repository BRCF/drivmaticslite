//
//  CMDeviceMotion.swift
//  DrivmaticsLite
//
//  Created by Bruno on 21/06/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved.
//

import CoreMotion

extension CMDeviceMotion {
    
    var userAccelerationInReferenceFrame: CMAcceleration {
        let acc = self.userAcceleration
        let rot = self.attitude.rotationMatrix
        
        var accRef = CMAcceleration()
        accRef.x = acc.x*rot.m11 + acc.y*rot.m12 + acc.z*rot.m13;
        accRef.y = acc.x*rot.m21 + acc.y*rot.m22 + acc.z*rot.m23;
        accRef.z = acc.x*rot.m31 + acc.y*rot.m32 + acc.z*rot.m33;
        
        return accRef;
    }
}

/*
 
 struct Axes {
    let x:Double = 0
    let y:Dounle = 0
    let z:Double = 0
 }
 
 public struct Matrix {
 
 
 public var m11: Double
 
 public var m12: Double
 
 public var m13: Double
 
 public var m21: Double
 
 public var m22: Double
 
 public var m23: Double
 
 public var m31: Double
 
 public var m32: Double
 
 public var m33: Double
 
 public init()
 
 public init(m11: Double, m12: Double, m13: Double, m21: Double, m22: Double, m23: Double, m31: Double, m32: Double, m33: Double)
 }
 
 func (T:Axes, )
 
 
 
 */
