//
//  LocatorInteractorBuilder.swift
//  GloboLive
//
//  Created by Bruno Castilho on 3/27/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved..
//

import Foundation

final class LocatorInteractorBuilder {
    
    static func make() -> LocatorInteractor  {
        
        let interactor = LocatorInteractor.init(manager: LocatorManager.instance)
        
        LocatorManager.instance.output?.addDelegate(interactor)
        
        return interactor
    }
}
