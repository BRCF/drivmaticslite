//
//  LocatorInteractor .swift
//  GloboLive
//
//  Created by Bruno Castilho on 3/27/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved..
//


import MapKit


protocol LocatorInteractorInput {
    func fetchLocation()
    func fetchStatus()
    func requestAuthorization()
    func startUpdatingLocation()
    func stopUpdating()
    func startTimerWith(interval:Double)
    func fetchListLocation() -> [CLLocation]
}


protocol LocatorInteractorOutput:class {
    func fetched(status:AuthorizationStatus)
    func fetchedLocation(item:LocationItem)
    func errorFetchingLocation()
    func errorFetchingAuthorization()
}


final class LocatorInteractor {
    
    weak var output:LocatorInteractorOutput?
    let manager:LocatorManager
    
    init(manager:LocatorManager) {
        self.manager = manager
    }
}

extension LocatorInteractor : LocatorInteractorInput {
    
    func requestAuthorization() {
        
        if self.manager.fetchStatus() == .denied || self.manager.fetchStatus() == .restricted {
            
            self.output?.errorFetchingAuthorization()
            
        } else {
            
            self.manager.requestAuthorization()
            
        }
        
    }
    
    func fetchStatus() {
        self.output?.fetched(status:self.manager.fetchStatus())
        
    }
    
    func fetchLocation() {
        
        if self.manager.isGeofencingAllowed() {
            
            self.manager.requestLocation()
            
        } else {
            
            self.output?.errorFetchingLocation()
            
        }
    }
    
    func startUpdatingLocation() {
        
        if self.manager.isGeofencingAllowed() {
            
            self.manager.startUpdatingLocation()
            
        } else {
            
            self.output?.errorFetchingLocation()
            
        }
    }
    
    func stopUpdating() {
        self.manager.stopObserver()
    }
    
    func fetchListLocation() -> [CLLocation] {
        return self.manager.locationList
    }
    
    func startTimerWith(interval:Double) {
        
        _ = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(LocatorInteractor.startTimer), userInfo: nil, repeats: true)
        
    }
    
    @objc func startTimer() {
        self.manager.requestLocation()
    }
    
}


extension LocatorInteractor:LocatorManagerOutput {
    
    func fetchedLocation(entity: locatorEntity) {
        self.output?.fetchedLocation(item: LocationItem.init(latitude: entity.latitude, longitude: entity.longitude, altitude: entity.altitude, horizontalAccuracy: entity.horizontalAccuracy, speed: entity.speed))
    }
    
    func failureToRequestLocation() {
        self.output?.errorFetchingLocation()
    }
    
}
