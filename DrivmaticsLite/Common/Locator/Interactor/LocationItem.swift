//
//  LocationEntity.swift
//  GloboLive
//
//  Created by Bruno Castilho on 4/3/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved..
//

import CoreLocation

class LocationItem {
    
    let latitude:Double
    let longitude:Double
    let speed:Double
    let altitude:Double
    let horizontalAccuracy:Double
    let timestamp:Date
    
    init(latitude:Double, longitude:Double, altitude:Double, horizontalAccuracy:Double, speed:Double, timestamp:Date = Date()) {
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.horizontalAccuracy = horizontalAccuracy
        self.timestamp = timestamp
        self.speed = speed
    }
    
}
