//
//  AuthorizationStatusEnum.swift
//  GloboLive
//
//  Created by Bruno Castilho on 4/10/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved..
//

public enum AuthorizationStatus {
    case notDetermined
    case restricted
    case denied
    case authorizedAlways
    case authorizedWhenInUse
}
