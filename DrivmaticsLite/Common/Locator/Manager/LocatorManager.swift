//
//  GeofencingManager.swift
//  GloboLive
//
//  Created by Bruno Castilho on 3/27/19.
//  Copyright © 2019 Bruno Raphael. All rights reserved..
//


import CoreLocation


struct locatorEntity {
    let latitude:Double
    let longitude:Double
    let speed:Double
    let altitude:Double
    let timestamp:Date
    let horizontalAccuracy:Double
}


protocol LocatorManagerOutput:class {
    func fetchedLocation(entity:locatorEntity)
    func failureToRequestLocation()
}

protocol LocatorManagerInput {
    func fetchStatus()-> AuthorizationStatus 
    func requestAuthorization()
    func isGeofencingAllowed()-> Bool
    func requestLocation()
    func startUpdatingLocation()
}

final class LocatorManager:NSObject, LocatorManagerInput {
    
    static let instance:LocatorManager = LocatorManager()
    var output:MulticastDelegate<LocatorManagerOutput>? = MulticastDelegate<LocatorManagerOutput>()
    var locationList: [CLLocation] = []
    var locationManager:CLLocationManager
    
    private override init() {
        self.locationManager  = CLLocationManager()
        super.init()
        
        self.locationManager.delegate = self
        locationManager.activityType = .automotiveNavigation
        locationManager.distanceFilter = kCLLocationAccuracyBest
        locationManager.allowsBackgroundLocationUpdates = true
        
    }
    
    func requestAuthorization() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func isGeofencingAllowed()-> Bool {
        return CLLocationManager.locationServicesEnabled() && (CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways)
    }
    
    func fetchStatus()-> AuthorizationStatus {
        
        switch CLLocationManager.authorizationStatus() {
            
        case .notDetermined:
            return AuthorizationStatus.notDetermined
            
        case .restricted:
            return AuthorizationStatus.restricted
            
        case .denied:
            return AuthorizationStatus.denied
            
        case .authorizedAlways:
            return AuthorizationStatus.authorizedAlways
            
        case .authorizedWhenInUse:
            return AuthorizationStatus.authorizedWhenInUse
            
        }
    }
    
    func requestLocation() {
        locationManager.requestLocation()
    }
    
    func startUpdatingLocation() {
        self.locationManager.startUpdatingLocation()
    }
    
    func stopObserver() {
        self.locationManager.stopUpdatingLocation()
    }
}


extension LocatorManager: CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.output?.invoke({$0.failureToRequestLocation()})
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard self.isGeofencingAllowed(), let location = manager.location else {
            self.output?.invoke({$0.failureToRequestLocation()})
            
            return
        }
        
        for newLocation in locations {
            
            let maxAge:TimeInterval = 270
            let requiredAccuracy:CLLocationAccuracy = 65
            
            let locationIsValid:Bool = Date().timeIntervalSince(newLocation.timestamp) < maxAge && newLocation.horizontalAccuracy <= requiredAccuracy
            
            if locationIsValid {
                
                locationList.append(newLocation)
                
                let speed = (location.speed * 3.6) > 0 ? (location.speed * 3.6) : 0
                
                self.output?.invoke({$0.fetchedLocation(entity:locatorEntity.init(latitude:newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude, speed: speed, altitude: location.altitude, timestamp: location.timestamp, horizontalAccuracy: location.horizontalAccuracy) )})
                
            }
        }
    }
}
